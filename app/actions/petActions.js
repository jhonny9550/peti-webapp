import { FETCH_PETS } from './types';
import { getPets } from '../utils/api';

export const fetchPets = () => (dispatch) => {
  console.log('Fetching pets ');
    dispatch({
      type: FETCH_PETS,
      payload: getPets()
    });
}
