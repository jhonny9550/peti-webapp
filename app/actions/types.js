export const FETCH_PETS = '[pet] fetch pets';
export const CREATE_PET = '[pet] create pet';
export const UPDATE_PET = '[pet] update pet';
export const DELETE_PET = '[pet] delete pet';