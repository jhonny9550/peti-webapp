import React from 'react';
import Navbar from './components/Navbar';
import PetList from './components/PetList';
import { Provider } from 'react-redux';
import { Divider } from 'semantic-ui-react';
import store from './store';

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <div>
          <Navbar />
          <Divider />
          <PetList />
        </div>
      </Provider>
    )
  }
}

export default App;
