import { CREATE_PET, DELETE_PET, FETCH_PETS, UPDATE_PET } from '../actions/types';

const initialState = {
  list: [],
  loading: null,
}

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_PETS:
      return { ...state, list: action.payload };
    default:
      return state;
  }
}