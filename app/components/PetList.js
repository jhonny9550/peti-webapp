import React from 'react';
import { Container } from 'semantic-ui-react';
import PetCard from './PetCard';
import { connect } from 'react-redux';
import { fetchPets } from '../actions/petActions';

class PetList extends React.Component {

  componentWillMount() {
    this.props.fetchPets();
  }

  render() {
    return (
      <Container>
        <div className="pet-container">
          {this.props.pets.map((pet, i) => <PetCard key={i} pet={pet}/>)}
        </div>
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  pets: state.pets.list
});

export default connect(mapStateToProps, { fetchPets })(PetList);