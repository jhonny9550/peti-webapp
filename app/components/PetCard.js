import React from 'react';
import { Card, Icon, Image } from 'semantic-ui-react';

class PetCard extends React.Component {

  constructor(props) {
    super();
    const { pet } = props;
    this.state = { ...pet };
  }

  render() {
    let favoriteFoods = 'This cat not have favorite foods';
    if (this.state.favFoods) {
      favoriteFoods += ': ';
      favoriteFoods += this.state.favFoods.join(', ');
      favoriteFoods += ' 😄';
    } else {
      favoriteFoods += ' 🙁';
    }
    return (
      <Card className="pet-card">
        <Image src={this.state.photo} />
        <Card.Content>
          <Card.Header>{this.state.name}</Card.Header>
          <Card.Meta>
            <span className='date'>{this.state.species}</span>
          </Card.Meta>
          <Card.Description dangerouslySetInnerHTML={{__html: favoriteFoods}}></Card.Description>
        </Card.Content>
        <Card.Content extra>
          <a>
            <Icon name='birthday' />
            {this.state.birthYear}
          </a>
        </Card.Content>
      </Card>
    )
  }
}

export default PetCard;