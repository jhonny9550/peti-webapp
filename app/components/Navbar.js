import React from 'react';
import { Header, Container } from 'semantic-ui-react';


class Navbar extends React.Component {
  render() {
    return (
      <Container className="custom-navbar">
        <Header as="h2">Hi! This is PETI.</Header>
        <p>A friendly app to adopt your next pet </p>
      </Container>
    )
  }
}

export default Navbar;